drop database hibernate;
create database hibernate;
use Hibernate;

drop table if exists `Account`;
create table `Account` (
                     id	        		int auto_increment primary key,
                     username			varchar(50) unique ,
                     email				varchar(50) not null,
                     password			varchar(50) not null,
                     first_name		    varchar(50),
                     last_name		    varchar(50),
                     `role`             enum('ADMIN', 'EMPLOYEE') not null
                     --`role`             int(1) -- Index của enum trong java -- 0: ADMIN, 2: EMPLOYEE
);

INSERT INTO `Hibernate`.`Account` (`username`, `email`, `password`, `first_name`, `last_name`, `role`) VALUES ('Nguyễn Văn A', 'a@gmail.com', '123456', 'Nguyễn Văn', 'A', 'ADMIN');
INSERT INTO `Hibernate`.`Account` (`username`, `email`, `password`, `first_name`, `last_name`, `role`) VALUES ('Nguyễn Văn B', 'b@gmail.com', '123456', 'Nguyễn Văn', 'B', 'EMPLOYEE');
INSERT INTO `Hibernate`.`Account` (`username`, `email`, `password`, `first_name`, `last_name`, `role`) VALUES ('Nguyễn Văn C', 'c@gmail.com', '123456', 'Nguyễn Văn', 'C', 'EMPLOYEE');


drop table if exists `Group`;
create table `Group` (
                           id	        		int auto_increment primary key,
                           name			varchar(50)
);

drop table if exists `Class`;
create table `Class` (
                         id	        		int auto_increment primary key,
                         name			varchar(25) not null unique,
                         create_date    date,
                         CLASS_FORMAT   enum('ONLINE', 'OFFLINE', 'HYBRID'),
                         address        varchar(100)
);



