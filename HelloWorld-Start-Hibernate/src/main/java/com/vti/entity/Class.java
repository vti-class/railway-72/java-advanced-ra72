package com.vti.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "`CLASS`")
@Getter
@Setter
@ToString // @Data
@NoArgsConstructor // tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khoải tạo đầy đủ tham số
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Tự động tăng giá trị ID
    @Column(name = "`ID`")
    private int id;

    @Column(name = "`name`", length = 25, nullable = false, unique = true)
    private String name;

    @Column(name = "`CREATE_DATE`")
    private Date createDate;

    @Column(name = "`CLASS_FORMAT`")
    @Enumerated(EnumType.ORDINAL)
    private ClassFormat classFormat;

    @Column(name = "ADDRESS", length = 100)
    private String address;
}
