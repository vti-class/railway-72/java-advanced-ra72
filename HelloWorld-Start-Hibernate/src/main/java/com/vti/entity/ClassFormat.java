package com.vti.entity;

public enum ClassFormat {
    ONLINE, OFFLINE, HYBRID
}
