package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.entity.Class;
import com.vti.entity.ClassFormat;
import com.vti.entity.Role;
import com.vti.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class ClassRepository implements IClassRepository{
    private Session session;

    public static void main(String[] args) throws SQLException {
        ClassRepository classRepository = new ClassRepository();


        // Thêm mới Lớp học
//        Class aClass = new Class();
//        aClass.setClassFormat(ClassFormat.HYBRID);
//        aClass.setAddress("19 Lê Thanh Nghị");
//        aClass.setCreateDate(new Date());
//        aClass.setName("Railway 73");
//
//        classRepository.createAccount(aClass);
        HibernateUtils.buildSessionFactory().openSession();

//        // Danh sách lớp học
//        List<Class> classes = classRepository.getAll();
//        for (Class v_Class: classes) {
//            System.out.println(v_Class);
//        }
    }
    @Override
    public List<Class> getAll() throws SQLException {
        session = HibernateUtils.buildSessionFactory().openSession();
        Query<Class> query = session.createQuery("FROM Class");
        return query.getResultList();
    }

    @Override
    public void createAccount(Class aClass) throws SQLException {

        session = HibernateUtils.buildSessionFactory().openSession();
        session.getTransaction().begin();
        session.save(aClass); // Cách 3: Sử dụng method của Hibernate -> Thực hiện các chức năng
        session.getTransaction().commit();
        System.out.println("Thêm mới thành công!");
    }

    @Override
    public void updateAccount(Class aClass) throws SQLException {

    }

    @Override
    public Class findById(int id) throws SQLException {
        return null;
    }

    @Override
    public void deleteById(int id) {

    }
}
