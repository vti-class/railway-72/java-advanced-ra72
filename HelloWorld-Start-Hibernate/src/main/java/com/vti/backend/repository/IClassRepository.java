package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.entity.Class;

import java.sql.SQLException;
import java.util.List;

public interface IClassRepository {
    List<Class> getAll() throws SQLException;

    void createAccount(Class aClass) throws SQLException;

    void updateAccount(Class aClass) throws SQLException;

    Class findById(int id) throws SQLException;

    void deleteById(int id);
}
